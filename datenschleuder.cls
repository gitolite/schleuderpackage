%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2013 koeart, john, ben                                         %
%                                                                          %
% This program is free software: you can redistribute it and/or modify it  %
% under the terms of the GNU General Public License as published by the    %
% Free Software Foundation, either version 3 of the License, or (at your   %
% option) any later version.                                               %
%                                                                          %
% This program is distributed in the hope that it will be useful, but      %
% WITHOUT ANY WARRANTY; without even the implied warranty of               %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        %
% General Public License for more details.                                 %
%                                                                          %
% You should have received a copy of the GNU General Public License along  %
% with this program.  If not, see <http://www.gnu.org/licenses/>.          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\ProvidesClass{datenschleuder}[%
	2013/07/19 v0.1 Datenschleuder
]
\LoadClass[%
	pdftex,
	a5paper,
	twoside,
	twocolumn,
	DIV=15,
]{scrartcl}

\RequirePackage[paper=a5paper,inner=20mm, outer=12mm,top=20mm,bottom=30mm]{geometry}

\RequirePackage{xkeyval}
\newcommand{\@volume}{}
\DeclareOptionX{volume}{\renewcommand{\@volume}{#1}}%
\newcommand{\@year}{}
\DeclareOptionX{year}{\renewcommand{\@year}{#1}}%
\ProcessOptionsX

\RequirePackage{ifluatex}
\ifluatex
	\RequirePackage{luatextra}
	\RequirePackage{lineno}
	\RequirePackage[EU2]{fontenc}
	\RequirePackage{fontspec}
	\setmainfont[Mapping=tex-text,Numbers={OldStyle}]{Linux Libertine O}
	\setsansfont[Mapping=tex-text,Numbers={OldStyle}]{Linux Biolinum O}
%	\setmonofont[Mapping=tex-text]{Courier Prime}
\fi

\RequirePackage{fmtcount}

\RequirePackage{scrpage2}
\RequirePackage{graphicx}
\RequirePackage{tikz}
\pagestyle{scrheadings}

\clearscrheadfoot
\setkomafont{pageheadfoot}{%
\normalfont\normalcolor\small
}

\rehead[]{\tikz[overlay]{\node[xshift=0.3mm,yshift=0.9mm]{\includegraphics[width=1cm]{Logo_CCC}};}}
\rohead[]{\tikz[overlay]{\node[xshift=0.3mm,yshift=0.9mm]{\includegraphics[width=1cm]{Logo_CCC}};}}
\chead{\directlua{tex.print(ruhetmp or "Bitte runninghead setzen")}}
\setheadsepline{0.5pt}

\ifoot[]{Datenschleuder. \@volume / \@year}
\ofoot[]{\texttt{0x\ifthenelse{\value{page}>15}{}{0}\hexadecimal{page}}}
\setfootsepline{0.5pt}

%FIXME: use abstract environment in prior of nasty DSabstract command
\renewenvironment{abstract}{%
	\begin{center}
		\bfseries
}{%
	\end{center}
}
\newcommand{\@DSabstract}{}
\newcommand{\DSabstract}[1]{\renewcommand{\@DSabstract}{#1}}


\renewcommand{\maketitle}{%
	\twocolumn[
	%	\renewcommand{\@runninghead}{\@runningheadtmp}
		\textsf{\Huge\@title}

		\begin{center}%
			\textsf{von \@author}%FIXME: implement mail and array of authors
		\end{center}

		{\bfseries\@DSabstract}
		\vspace*{3mm}
	]
		\directlua{ruhetmp=nxruhetmp nxruhetmp=nil}
	\normalsize
}

\define@key{DSarticleKeys}{title}{\title{#1}}%
\define@key{DSarticleKeys}{author}{\author{#1}}
\define@key{DSarticleKeys}{DSabstract}{\renewcommand{\@DSabstract}{#1}}
%\newcommand{\@runninghead}{}
%\newcommand{\@runningheadtmp}{}
%\newcommand{\runninghead}[1]{\renewcommand{\@runningheadtmp}{#1}}
\newcommand{\runninghead}[1]{\directlua{nxruhetmp = "\luatexluaescapestring{#1}"}}
\newcommand{\manrunninghead}[1]{\directlua{ruhetmp = "\luatexluaescapestring{#1}"}}
\define@key{DSarticleKeys}{runninghead}[\@title]{\runninghead{#1}}

\newenvironment{DSarticle}[1][]{%
	\setkeys{DSarticleKeys}{#1}%
	\maketitle
}{}

\newcounter{NumOfLetters}
\setcounter{NumOfLetters}{0}

\newenvironment{letter}{%
	\setlength{\parskip}{2.5mm}%
	\setlength{\parindent}{0mm}%
	\ifthenelse{\equal{\arabic{NumOfLetters}}{0}}{}{%
		\vspace{-3mm}%
		\begin{center}%
			\hfill%
			\hfill%
			\includegraphics[width=5mm]{pesthoernchen}%
			\hfill%
			\includegraphics[width=5mm]{pesthoernchen}%
			\hfill%
			\includegraphics[width=5mm]{pesthoernchen}%
			\hfill%
			\hfill\hspace*{0cm}%
		\end{center}%
		\vspace{-5mm}%
	}%
	\stepcounter{NumOfLetters}%
}{%
}
\newenvironment{question}{%
}{%
}
\newenvironment{answer}{%
		\itshape%
}{%
}
